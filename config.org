
* My private configuration
** doom defaults
#+begin_src emacs-lisp
;; $DOOMDIR/config.el -*- lexical-binding: t; -*-
#+end_src

Place your private configuration here! Remember, you do not need to run 'doom
sync' after modifying this file!

Some functionality uses this to identify you, e.g. GPG configuration, email
clients, file templates and snippets.

#+begin_src emacs-lisp
(setq user-full-name "Denys Levytsky"
      user-mail-address "denys.levytsky@gmail.com")
#+end_src

Doom exposes five (optional) variables for controlling fonts in Doom. Here
are the three important ones:
 + `doom-font'
 + `doom-variable-pitch-font'
 + `doom-big-font' -- used for `doom-big-font-mode'; use this for presentations or streaming.
They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
font string. You generally only need these two:

#+begin_src emacs-lisp
(setq doom-font (font-spec :family "Iosevka" :size 14)
      doom-variable-pitch-font (font-spec :family "Iosevka" :size 14))
#+end_src

There are two ways to load a theme. Both assume the theme is installed and
available. You can either set `doom-theme' or manually load a theme with the
`load-theme' function. This is the default:

#+begin_src emacs-lisp
(setq doom-theme 'doom-gruvbox-light)
;; (setq doom-theme 'doom-vibrant)
;; (setq doom-theme 'doom-one)
#+end_src

If you use `org' and don't want your org files in the default location below,
change `org-directory'. It must be set before org loads!

#+begin_src emacs-lisp
(setq org-directory "~/org/")
#+end_src

This determines the style of line numbers in effect. If set to `nil', line
numbers are disabled. For relative line numbers, set this to `relative'.

#+begin_src emacs-lisp
(setq display-line-numbers-type t)
#+end_src


Here are some additional functions/macros that could help you configure Doom:

- `load!' for loading external *.el files relative to this one
- `use-package' for configuring packages
- `after!' for running code after a package has loaded
- `add-load-path!' for adding directories to the `load-path', relative to
   this file. Emacs searches the `load-path' when you load packages with `require' or `use-package'.
- `map!' for binding new keys
To get information about any of these functions/macros, move the cursor over
the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
This will open documentation for it, including demos of how they are used.

You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
they are implemented.

** defaults
Enable autosave for files. Autosave triggers after certain number of characters were typed or after a period of inactivity.
#+begin_src emacs-lisp
(setq auto-save-default t)
#+end_src


Display time in the right side of modeline.
#+begin_src emacs-lisp
(display-time-mode 1)
(setq display-time-24hr-format t)
#+end_src

Set default mode for new buffers to org instead of fundamental.
#+begin_src emacs-lisp
(setq-default major-mode 'org-mode)
#+end_src

** go-mode

#+begin_src emacs-lisp
(use-package go-mode
  :mode "\\.go\\'"
  :config
  (add-hook 'go-mode-hook 'lsp-go-install-save-hooks))
#+end_src

#+begin_src emacs-lisp
;; (require 'lsp-mode)
;; (add-hook 'go-mode-hook #'lsp-deferred)
#+end_src

#+begin_src emacs-lisp
;; Set up before-save hooks to format buffer and add/delete imports.
;; Make sure you don't have other gofmt/goimports hooks enabled.
(defun lsp-go-install-save-hooks ()
  (interactive)
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
;; (add-hook 'go-mode-hook #'lsp-go-install-save-hooks)
#+end_src

** evil-snipe

Allow snipe to work with whole buffer.
#+begin_src emacs-lisp
(setq evil-snipe-scope 'whole-buffer)
(setq evil-snipe-repeat-scope 'whole-buffer)
#+end_src

** eshell

#+begin_src emacs-lisp
;; (setq company-global-modes '(not eshell-mode))

;; (defun clear-scrollback ()
;;   (interactive)
;;   "Clear the scrollback content of the eshell window."
;;   (let ((inhibit-read-only t))
;;     (erase-buffer))
;;     (eshell-send-input))

;; (map! :map eshell-mode-map :i "C-l" #'clear-scrollback)
#+end_src

** vterm

#+begin_src emacs-lisp
(setq vterm-shell "/usr/bin/fish")
(setq vterm--prompt-tracking-enabled-p t)
(map! :map vterm-mode-map "jk" #'evil-escape)
#+end_src


** avy
Use avy search across all visible windows.
#+begin_src emacs-lisp
(setq avy-all-windows t)
#+end_src

** transparent window settings

Setting for making emacs frame transparent.
#+begin_src emacs-lisp
;; (set-frame-parameter (selected-frame) 'alpha '(90 90))
;; (add-to-list 'default-frame-alist '(alpha 90 90))
#+end_src

** keymap

#+begin_src emacs-lisp
(setq-default evil-escape-key-sequence "jk")
#+end_src

** org

Set agenda files.
#+begin_src emacs-lisp
(setq org-agenda-files '("~/Dev/notes/org/agenda/todo.org" "~/Dev/notes/org/agenda/news.org"))
#+end_src

Remove header line 'Edit, then exit with 'C-c C-c' or abort with 'C-c C-k'
#+begin_src emacs-lisp
;; (eval-after-load 'org-capture
;;    (progn
;;       (defun org-capture-turn-off-header-line ()
;;          (setq-local header-line-format nil))

;;       (add-hook 'org-capture-mode-hook #'org-capture-turn-off-header-line)))
;; (defun org-capture-turn-off-header-line ()
;;   (setq-local header-line-format nil))

;; (add-hook 'org-capture-mode-hook #'org-capture-turn-off-header-line)
#+end_src

***  roam

#+begin_src emacs-lisp
(setq org-roam-directory "~/Dev/notes/roam")
(setq org-roam-v2-ack t)
#+end_src

** rg

#+begin_src emacs-lisp
(rg-enable-default-bindings)
#+end_src
